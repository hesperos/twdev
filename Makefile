.PHONY: server

all:
	NODE_PATH=${PWD}/node_modules:${NODE_PATH} hugo

server:
	NODE_PATH=${PWD}/node_modules:${NODE_PATH} hugo-extend server --disableFastRender
