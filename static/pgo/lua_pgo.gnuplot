#!/usr/bin/env gnuplot

set term png size 800,600
set title "Lua benchmark results"

set boxwidth 0.9
set datafile separator ","
set style fill transparent solid 0.5
set yrange [8:*]

set xlabel "command"
set ylabel "seconds"
set xtics rotate by 315 noenhanced

set grid ytics mytics
set mytics 2
set grid

plot "lua_vanilla_combined.csv" using 0:2:xtic(1) with boxes lc rgb 'blue' title 'Lua vanilla', \
         '' using 0:2:7:8 with yerrorbars lc rgb 'black' pt 1 lw 2 notitle, \
        "lua_pgo_combined.csv" using 0:2:xtic(1) with boxes lc rgb 'red' title 'Lua with PGO', \
        '' using 0:2:7:8 with yerrorbars lc rgb 'black' pt 1 lw 2 notitle
