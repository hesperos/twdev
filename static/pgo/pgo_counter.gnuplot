#!/usr/bin/env gnuplot

set term png size 800,600
set title "counter.sh benchmarks"

set boxwidth 0.9
set datafile separator ","
set style fill solid
set yrange [2:*]

unset key
set xlabel "command"
set ylabel "seconds"
set xtics rotate by 315 noenhanced

set grid ytics mytics
set mytics 2
set grid

plot "bash_pgo_counter_combined.csv" using 0:2:xtic(1) with boxes, '' using 0:2:7:8 with yerrorbars lc rgb 'black' pt 1 lw 2
