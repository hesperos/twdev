---
title: "About"
date: 2021-07-03T23:59:47+01:00
draft: false
tags: []
---

![t.w portrait](/images/p2.jpg)

We are living in a dynamic world. Technology changes all the time and impacts
our lives.  Every technology comes with its own problems and quirks.  It takes
time and perseverance to master all the details and transform a set of
utilities into a successful commercial product.

During my career I've designed and written software for commercially successful
devices oriented for ISP market for companies like [Nokia Siemens
Networks](https://en.wikipedia.org/wiki/Nokia_Networks) or
[ADTRAN](https://adtran.com/).  This required low-level work with Linux kernel
and custom hardware drivers, as well as complex distributed system software,
orchestrating operations between multiple hardware cards comprising a full
system.

Next chapter of my career focussed on customer premises network and multimedia
devices.  This includes a range of CPE devices like Sky Hub and SkyQ
set-top-box from [SKY](https://sky.com) as well as a range of set-top-boxes sold in
retail and distributed by ISPs like BT or TalkTalk running unique
[YouView](https://youview.com) software stack.  Working with set-top-boxes
required focus on a different problems domain and using different technologies.
A reliable, fast and distributed middleware built in C++ guarantees great user
experience and, at the same time, allows to progress development in an agile
manner.

All these products were successfully deployed to milions of homes within United
Kingdom serving its purpose well.

This blog is a place where I try to catalogue some of the discoveries and
lessons learned from my professional career.  Meant for myself so, I don't
forget but, at the same time, maybe useful for other people.

Please get in touch through my social media if you like what I do.
