---
title: "Testing in golang with \"Is\" framework"
date: 2021-08-14T16:02:09+01:00
draft: false
tags: [ "golang", "testing", "is", "framework" ]
---

Today's post is gonna be a short one.  I'd like to recommend a framework which
I used personally on a number of occasions.

Golang comes with a great environment and a set of tools.  Tests are first class citizens.  Why would you need additional framework for your tests then?  Purely for convenience.

## [is](https://github.com/matryer/is) API

[is](https://github.com/matryer/is) is a mini framework, at the moment of writing this post, the API is comprised of four functions:

- `is.Equal`
- `is.True`
- `is.NoErr`
- `is.Fail`

and frankly, majority of the times, that's all you'll need.  The framework itself doesn't distract you from the work, yet provides convenience to write tests faster and express the intention clearer.  Compare the following:

```golang
func TestIfReturnsCorrectResults(t *testing.T) {
	is := is.New(t)
	is.Equal(1+2, add(1, 2))
}

func TestIfReturnsCorrectResults2(t *testing.T) {
	expected := 1 + 2
	if res := add(1, 2); res != expected {
		t.Fatalf("%d != %d", res, expected)
	}
}
```

I'm sure we all agree that the former is much more concise and quicker to write.  

## API documentation

Tests usually serve two purposes.  The first one is obvious: quality assurance.  The second one is API documentation.  As far as the latter is concerned, [is](https://github.com/matryer/is) serves that purpose perfectly.  Tests written with [is](https://github.com/matryer/is) assertions can be, more or less, read like a natural spoken language which greatly improves comprehension.

Give [is](https://github.com/matryer/is) a try.  I'm pretty sure you won't regret it.
