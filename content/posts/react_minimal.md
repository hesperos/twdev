---
title: "Minimal React project template"
date: 2024-01-03T19:35:56Z
draft: false
tags: ["react","parcel","npm"]
---

Although it is strongly criticised for its bloat,
[create-react-app](https://create-react-app.dev/) is often a go to when starting
a new React project.  I like to start things from the bottom up, adding things
up as I need them so, I prefer a minimalistic approach.

It's actually very easy to setup a bare-bones React project using
[parcel](https://parceljs.org/).  Here's how to do it.

## Initialise npm project

    mkdir react-minimal
    cd react-minimal
    npm init -y
    mkdir src

Some adjustments are needed to `package.json`.  I'm gonna change the `main` to
`source`, pointing to the `index.html` of my project.  I'm gonna create an
empty `src/index.html` for the interim.

    touch src/index.html

Here's the resulting package.json.

```json
{
  "name": "react-minimal",
  "version": "1.0.0",
  "description": "",
  "source": "src/index.html",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
  },
  "dependencies": {
  }
}

```

## Add parcel

I'm using `parcel` as build system and bundler.

    npm install --save-dev parcel

## Add React

Obviously, I'm adding React as dependency

    npm install react react-dom

## Sample application

I'm gonna start with the `App` component:

```javascript
import React from 'react';

export default function App() {
    return (
        <div>
            <h1>Hello world!</h1>
        </div>
    );
}
```

I'm using it in `index.jsx` which is my entry point.

```javascript
import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';

const container = document.getElementById('root');

const root = ReactDOM.createRoot(container);
root.render(<App />, container);
```

This is included in `index.html`.


```html
<html>
<head>
    <title>Minimal React application</title>
</head>
<body>
    <div id="root"></div>
    <script type="module" src="index.jsx"></script>
</body>
</html>
```

This can be rendered with `parcel`.

```bash
$ npx parcel src/index.html 
Server running at http://localhost:1234
✨ Built in 710ms
```

Or bundled using the `build` command

```bash
parcel build
```

That's it! 

## Babel and JSX translation

You don't need Babel at all as [parcel is taking care of JSX translation](https://en.parceljs.org/transforms.html) out of the box.

## Example code

Code discussed in this post is available in [gitlab repo](https://gitlab.com/twdev_projects/react-minimal).
