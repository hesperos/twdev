---
title: "Implementing a range tree in C++"
date: 2021-08-05T08:25:45+01:00
draft: true
tags: [ "c++", "interview", "datastructures", "rangetree", "tree", "visitor" ]
---

Range trees are an interesting data structure allowing to efficiently return a set of data points residing within a provided interval.  In other words, the query contains an interval and it results in a set of points.

## Rationale

Think of it as a specialised data structure perfect for a database cache.  Assuming that there's a data set that is queried often from the DB, a range tree could be built on demand acting as a cache, minimising the database load.  It's a perfect solution for read-heavy problems where the initial data set doesn't change often.  Why not rely strictly on redis or memcached?  Most of the time that's perfect but sometimes multilayer caching can give even better results.

In this article I won't refer heavily to any practical application.  I'm more interested in the data structure itself and a practical approach to implement it.  So, join me for the ride, we'll have some C++ fun!

## Principles

Range Tree in its essence is a binary search tree with a twist.  Leaf nodes store the actual values.  Internal Nodes are special.  Each internal node stores the maximal value of its left subtree.  [wiki](https://en.wikipedia.org/wiki/Range_tree) article describes the details behind the tree structure, which I strongly suggest to read.  The first step will be to define the data types representing the nodes.

### Nodes representation

I'll start by defining an abstract interface defining an operations on a Node:

```C++
template <typename DataT>
class Node {
public:
    virtual ~Node() = default;

    virtual DataT get() const = 0;

    virtual DataT max() const = 0;
};
```

So far so good.  `get()` allows to retrieve the value from the node.  In case of leafs it will be the actual data, in case of internal nodes, as already explained, it will be the maximum value in left subtree.  I've added `max()` for convenience, as it will become apparent later on.  It allows to retrieve the maximum value from the subtree rooted at given node.

Concrete implementation of a leaf node is defined the following way:

```C++
template <typename DataT>
class Leaf : public Node<DataT> {
public:
    explicit Leaf(DataT value) :
        value{std::move(value)}
    {
    }

    DataT get() const override {
        return value;
    }

    DataT max() const override {
        return value;
    }

private:
    DataT value;
};
```

Simple, self explanatory stuff.  Internal nodes are a bit more interesting:

```C++
template <typename DataT>
class Internal : public Node<DataT> {
public:
    Internal(std::unique_ptr<Node<DataT>> left, std::unique_ptr<Node<DataT>> right) :
        left{std::move(left)},
        right{std::move(right)}
    {
    }

    DataT get() const override {
        return left->max();
    }

    DataT max() const override {
        if (!left && !right) {
            throw std::runtime_error("Internal Node without any children");
        }

        if (!left) {
            return right->max();
        }

        if (!right) {
            return left->max();
        }

        return std::max(left->max(), right->max());
    }

private:
    std::unique_ptr<Node<DataT>> left;
    std::unique_ptr<Node<DataT>> right;
};
```

The bulk of the work is done in the `max()` function, which calculates the maximum value within the subtree recursively.  Having this function, implementation of `get()` becomes trivial.

The aforementioned three data types will allow to represent the entire tree.  It's now possible to focus on the construction.

### Construction of the tree

Range tree is a special case of the binary search tree and as with any binary search tree the input data decides on the structure of the resulting tree.  In other words, to land up with a balanced tree, the input data has to be sorted.  I'll construct the tree in two steps:

- sort input data set (O(n log n)),
- recursively build the tree.

```C++
template <typename F, typename L>
    static std::unique_ptr<Node<DataT>> create(F first, L last) {
        std::sort(first, last);
        return create_(first, last);
    }
```

This function takes two iterators.  Sorts the input vector in place and moves forward to actual tree construction:

```C++
template <typename F, typename L>
    static std::unique_ptr<Node<DataT>> create_(F first, L last) {
        const auto d = std::distance(first, last);
        if (d <= 0) {
            return std::unique_ptr<Node<DataT>>();
        }
        if (d == 1) {
            return std::make_unique<Leaf<DataT>>(*first);
        }
        const auto middle = d / 2;
        auto left = create_(first, first + middle);
        auto right = create_(first + middle, last);
        return std::make_unique<Internal<DataT>>(std::move(left), std::move(right));
    }
```

Construction happens recursively in four steps:

- determine the middle (median) element of the input vector (the pivot),
- divide the input vector into two subsets: left - containing values < pivot, and right - containing values > pivot,
- recursively construct the tree for left and right subsets,
- create internal node merging left and right subtrees.

This is the simplest way to construct the tree recursively.  It could be done differently of course.  It's possible to use AVL or red black tree or any other self balancing tree data structure as a back-end and simply insert data into it one element at a time.  Initial pre-sorting wouldn't be a prerequisite in such case.

With my implementation, to make my life easier, I've made a couple of assumptions:

- The tree is an immutable data structure, hence I don't implement modification operations and as a result I don't care about re-balancing the tree or using a self-balancing tree to begin with,
- I'm not gonna try to implement STL style iterators or attempt to create a generic container since that would distract from the data structure itself.

## Traversal

This is the fundamental question.  I've got the tree built, how do I get any data out of it?  For this purpose, I'll implement a good old [visitor](https://en.wikipedia.org/wiki/Visitor_pattern) pattern.  A good place to start is to define the visitor's interface:

```C++
template <typename DataT> class Leaf;
template <typename DataT> class Internal;

template <typename DataT>
class Visitor {
public:
    virtual ~Visitor() = default;

    virtual void visit(const Leaf<DataT>&) = 0;

    virtual void visit(const Internal<DataT>&) = 0;
};
```

Simple stuff.  Any visitor can accept either a leaf or internal node.  To support visitors, the `Node<DataT>` interface has to be slightly extended as well.  I'll add one simple function: `accept()`.

```C++
template <typename DataT>
class Node {
public:
    ...
    virtual void accept(Visitor<DataT>&) const = 0;
};
```

Both, the leaf and an internal node will implement it by doing a dispatch to the visitor on themselves:

```C++
void accept(Visitor<DataT>& v) const override {
    v.visit(*this);
}
```

Now, it's possible to implement concrete visitors, capable of the tree
traversal.
