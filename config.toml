baseURL = "https://twdev.blog"
languageCode = "en-gb"
title = "twdev.blog"
paginate = 7

theme = "loveit"
canonifyURLs = true
enableGitInfo = true
toc = true
editor = "vim"
enableRobotsTXT = true
pygmentsstyle = "tango"
PygmentsCodeFences = true
enableInlineShortcodes = true

[params]
    share = true
    related = "all"
    img = "capsule-corp.png"
    version = "0.2.X"
    defaultTheme = "light"
    description = "C++ & Golang software engineering blog by Tomasz Wisniewski.  Lessons learned and tips and tricks about modern software development with C++ and Golang."

    [params.header]
        desktopMode = "fixed"
        mobileMode = "auto"
        [params.header.title]
        logo = ""
        name = "twdev.blog"
        pre = '<span><i class="fas fa-code-branch"></i></span> ['
        post = "]"
        typeit = false

    [params.footer]
        enable = true
        custom = 'License restrictions apply to all content.'
        hugo = false
        copyright = true
        author = true
        since = 2021
        icp = ""
        license = '<a rel="license external nofollow noopener noreffer" href="https://creativecommons.org/licenses/by-nc/4.0/" target="_blank">CC BY-NC 4.0</a>'

    [params.social]
        GitHub = "dagon666"
        Linkedin = "tomasz-wisniewski-14350556"
        Twitter = "wisnitom"
        Gitlab = "hesperos"
        Youtubelegacy = ""
        Youtubecustom = ""
        Youtubechannel = "UCmRNem_HymcO54Y4wAvtmGg"
        Reddit = ""
        Codepen = ""
        FreeCodeCamp = ""
        Stackoverflow = ""
        Flickr = ""
        Paypal = ""
        Patreon = ""
        Twitch = ""
        RSS = true

    [params.home]
        rss = 10

        [params.home.profile]
            enable = true
            # Gravatar Email for preferred avatar in home page
            gravatarEmail = ""
            # URL of avatar shown in home page
            avatarURL = "/images/p1.jpg"
            title = "Hi, I'm Tomasz"
            subtitle = "I'm a professional C++ software engineer with over a decade of hands on development experience with variety of technologies (mostly Linux & embedded systems). This is my blog."
            typeit = false
            social = true
            disclaimer = "Find me on social media"
          [params.home.posts]
            enable = true
            paginate = 7
            defaultHiddenFromHomePage = false

    [params.page]
        hiddenFromHomePage = false
        hiddenFromSearch = false
        twemoji = false
        lightgallery = false
        ruby = true
        fraction = true
        fontawesome = true
        linkToMarkdown = true
        rssFullText = false

        [params.page.kofi]
            identifier = 'V7V8F8LLF'

        [params.page.toc]
            enable = true
            keepStatic = false
            auto = true

        [params.page.math]
            enable = true
            blockLeftDelimiter = ""
            blockRightDelimiter = ""
            inlineLeftDelimiter = ""
            inlineRightDelimiter = ""
            copyTex = true
            mhchem = true

        [params.page.code]
            copy = true
            maxShownLines = 128

        [params.page.share]
            enable = true
            Twitter = true
            Facebook = true
            Linkedin = true
            Whatsapp = false
            Pinterest = false
            Tumblr = false
            HackerNews = true
            Reddit = true
            VK = false
            Buffer = false
            Xing = false
            Line = false
            Instapaper = false
            Pocket = false
            Digg = false
            Stumbleupon = false
            Flipboard = false
            Weibo = false
            Renren = false
            Myspace = false
            Blogger = true
            Baidu = false
            Odnoklassniki = false
            Evernote = true
            Skype = false
            Trello = true
            Mix = false

        [params.page.comment]
            enable = true

            [params.page.comment.disqus]
                enable = true
                shortname = "twdev-blog"

            [params.page.comment.gitalk]
                enable = false
                owner = ""
                repo = ""
                clientId = ""
                clientSecret = ""

            [params.page.comment.telegram]
                enable = false
                siteID = ""
                limit = 5
                height = ""
                color = ""
                colorful = true
                dislikes = false
                outlined = false

            [params.page.comment.utterances]
                enable = false
                repo = ""
                issueTerm = "pathname"
                label = ""
                lightTheme = "github-light"
                darkTheme = "github-dark"

        [params.page.library]
            [params.page.library.css]
                # located in "assets/"
                # someCSS = "some.css"
                # someCSS = "https://cdn.example.com/some.css"
            [params.page.library.js]
                # located in "assets/"
                # someJavascript = "some.js"
                # someJavascript = "https://cdn.example.com/some.js"

        [params.page.seo]
            # image URL
            images = []
            [params.page.seo.publisher]
                name = "Tomasz Wisniewski"
                logoUrl = ""

    # posts
    [params.section]
      paginate = 20
      dateFormat = "01-02"
      rss = 10

    # tags
    [params.list]
      paginate = 20
      dateFormat = "01-02"
      rss = 10

    [params.cookieconsent]
      enable = true
      [params.cookieconsent.content]
      message = "This blog is using cookies."
      dismiss = "OK"
      link = ""





# Site verification code config for Google/Bing/Yandex/Pinterest/Baidu
[params.verification]
  google = ""
  bing = ""
  yandex = ""
  pinterest = ""
  baidu = ""

  # LoveIt NEW | 0.2.10 Site SEO config
  [params.seo]
    # image URL
    image = ""
    # thumbnail URL
    thumbnailUrl = ""

  # LoveIt NEW | 0.2.0 Analytics config
  [params.analytics]
    enable = true
    # Google Analytics
    [params.analytics.google]
      id = "G-XDFK89KERR"
      # whether to anonymize IP
      anonymizeIP = true
    # Fathom Analytics
    [params.analytics.fathom]
      id = ""
      # server url for your tracker if you're self hosting
      server = ""

  # LoveIt CHANGED | 0.2.7 CDN config for third-party library files
  [params.cdn]
    # CDN data file name, disabled by default
    # ("jsdelivr.yml")
    # located in "themes/LoveIt/assets/data/cdn/" directory
    # you can store your own data files in the same path under your project:
    # "assets/data/cdn/"
    data = ""

  # LoveIt NEW | 0.2.8 Compatibility config
  [params.compatibility]
    # whether to use Polyfill.io to be compatible with older browsers
    polyfill = false
    # whether to use object-fit-images to be compatible with older browsers
    objectFit = false


[markup]
  [markup.highlight]
    codeFences = true
    guessSyntax = true
    lineNos = true
    lineNumbersInTable = true
    noClasses = false
  [markup.goldmark]
    [markup.goldmark.extensions]
      definitionList = true
      footnote = true
      linkify = true
      strikethrough = true
      table = true
      taskList = true
      typographer = true
    [markup.goldmark.renderer]
      unsafe = false
  [markup.tableOfContents]
    startLevel = 2
    endLevel = 6

[author]
  name = "Tomasz Wisniewski"
  email = ""
  link = ""

[sitemap]
  changefreq = "weekly"
  filename = "sitemap.xml"
  priority = 0.5

[Permalinks]
  posts = ":year/:month/:filename"

[mediaTypes]
  [mediaTypes."text/plain"]
    suffixes = ["md"]

[outputFormats.MarkDown]
  mediaType = "text/plain"
  isPlainText = true
  isHTML = false

[outputs]
  home = ["HTML", "RSS", "JSON"]
  page = ["HTML", "MarkDown"]
  section = ["HTML", "RSS"]
  taxonomy = ["HTML", "RSS"]
  taxonomyTerm = ["HTML"]








# Privacy config
[privacy]
  # LoveIt DELETED | 0.2.0 privacy of the Google Analytics (replaced by params.analytics.google)
  [privacy.googleAnalytics]
    # ...
  [privacy.twitter]
    enableDNT = true
  [privacy.youtube]
    privacyEnhanced = true



[[menu.main]]
	identifier = "about"
	name = "About"
	url = "/about/"
	weight = 1

[[menu.main]]
	identifier = "posts"
	name = "Archives"
	url = "/posts/"
	weight = 2

[[menu.main]]
	identifier = "tags"
	name = "Tags"
	url = "/tags/"
	weight = 3
